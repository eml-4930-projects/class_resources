import os
from setuptools import setup
from glob import glob

package_name = 'gokart_gazebo'

# Common files that might be included with a package
data_files = [
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        ('share/' + package_name + '/launch', glob('launch/*.launch.py')),
        ('share/' + package_name + '/rviz', glob('rviz/*.rviz')),
    ]

# Have to do this because of gazebo files
for root, subdirs, files in os.walk('gazebo'):
    if not files == []:
        data_files.append((os.path.join('share', package_name, root), [os.path.join(root, file) for file in files]))

setup(
    name=package_name,
    version='0.0.2',
    packages=[package_name],
    data_files=data_files,
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Patrick Neal',
    maintainer_email='neap@ufl.edu',
    description='Contains useful launch files, configuration files, and gazebo files for use in the EML4930',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
        ],
    },
)
