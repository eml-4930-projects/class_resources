<?xml version="1.0" ?>
<sdf version="1.8">
  <world name="navsat_test">

    <!-- Processes the NavSat sensor -->
    <plugin
      filename="ignition-gazebo-navsat-system"
      name="ignition::gazebo::systems::NavSat">
    </plugin>

    <!-- Handles requests to update the spherical coordinates -->
    <plugin
      filename="ignition-gazebo-user-commands-system"
      name="ignition::gazebo::systems::UserCommands">
    </plugin>

    <!-- Populates the GUI with data from the server -->
    <plugin
      filename="ignition-gazebo-scene-broadcaster-system"
      name="ignition::gazebo::systems::SceneBroadcaster">
    </plugin>

    <!-- Processes pose commands -->
    <plugin
      filename="ignition-gazebo-physics-system"
      name="ignition::gazebo::systems::Physics">
    </plugin>

    <!-- Set the coordinates for the world origin -->
    <spherical_coordinates>
      <surface_model>EARTH_WGS84</surface_model>
      <world_frame_orientation>ENU</world_frame_orientation>
      <latitude_deg>-22.986687</latitude_deg>
      <longitude_deg>-43.202501</longitude_deg>
      <elevation>0</elevation>
      <heading_deg>0</heading_deg>
    </spherical_coordinates>

    <gui fullscreen="0">

      <!-- 3D scene -->
      <plugin filename="MinimalScene" name="3D View">
        <ignition-gui>
          <title>3D View</title>
          <property type="bool" key="showTitleBar">false</property>
          <property type="string" key="state">docked</property>
        </ignition-gui>

        <engine>ogre2</engine>
        <scene>scene</scene>
        <ambient_light>0.4 0.4 0.4</ambient_light>
        <background_color>0.8 0.8 0.8</background_color>
        <camera_pose>0 -6 8 0 0.8 1.56</camera_pose>
      </plugin>

      <!-- Plugins that add functionality to the scene -->
      <plugin filename="EntityContextMenuPlugin" name="Entity context menu">
        <ignition-gui>
          <property key="state" type="string">floating</property>
          <property key="width" type="double">5</property>
          <property key="height" type="double">5</property>
          <property key="showTitleBar" type="bool">false</property>
        </ignition-gui>
      </plugin>
      <plugin filename="GzSceneManager" name="Scene Manager">
        <ignition-gui>
          <property key="resizable" type="bool">false</property>
          <property key="width" type="double">5</property>
          <property key="height" type="double">5</property>
          <property key="state" type="string">floating</property>
          <property key="showTitleBar" type="bool">false</property>
        </ignition-gui>
      </plugin>
      <plugin filename="InteractiveViewControl" name="Interactive view control">
        <ignition-gui>
          <property key="resizable" type="bool">false</property>
          <property key="width" type="double">5</property>
          <property key="height" type="double">5</property>
          <property key="state" type="string">floating</property>
          <property key="showTitleBar" type="bool">false</property>
        </ignition-gui>
      </plugin>
      <plugin filename="CameraTracking" name="Camera Tracking">
        <ignition-gui>
          <property key="resizable" type="bool">false</property>
          <property key="width" type="double">5</property>
          <property key="height" type="double">5</property>
          <property key="state" type="string">floating</property>
          <property key="showTitleBar" type="bool">false</property>
        </ignition-gui>
      </plugin>
      <plugin filename="SelectEntities" name="Select Entities">
        <ignition-gui>
          <property key="resizable" type="bool">false</property>
          <property key="width" type="double">5</property>
          <property key="height" type="double">5</property>
          <property key="state" type="string">floating</property>
          <property key="showTitleBar" type="bool">false</property>
        </ignition-gui>
      </plugin>
      <plugin filename="Spawn" name="Spawn Entities">
        <ignition-gui>
          <property key="resizable" type="bool">false</property>
          <property key="width" type="double">5</property>
          <property key="height" type="double">5</property>
          <property key="state" type="string">floating</property>
          <property key="showTitleBar" type="bool">false</property>
        </ignition-gui>
      </plugin>
      <!-- World control -->
      <plugin filename="WorldControl" name="World control">
        <ignition-gui>
          <title>World control</title>
          <property type="bool" key="showTitleBar">false</property>
          <property type="bool" key="resizable">false</property>
          <property type="double" key="height">72</property>
          <property type="double" key="width">121</property>
          <property type="double" key="z">1</property>

          <property type="string" key="state">floating</property>
          <anchors target="3D View">
            <line own="left" target="left"/>
            <line own="bottom" target="bottom"/>
          </anchors>
        </ignition-gui>

        <play_pause>true</play_pause>
        <step>true</step>
        <start_paused>true</start_paused>
        <use_event>true</use_event>

      </plugin>

      <!-- World statistics -->
      <plugin filename="WorldStats" name="World stats">
        <ignition-gui>
          <title>World stats</title>
          <property type="bool" key="showTitleBar">false</property>
          <property type="bool" key="resizable">false</property>
          <property type="double" key="height">110</property>
          <property type="double" key="width">290</property>
          <property type="double" key="z">1</property>

          <property type="string" key="state">floating</property>
          <anchors target="3D View">
            <line own="right" target="right"/>
            <line own="bottom" target="bottom"/>
          </anchors>
        </ignition-gui>

        <sim_time>true</sim_time>
        <real_time>true</real_time>
        <real_time_factor>true</real_time_factor>
        <iterations>true</iterations>
      </plugin>

      <!-- Translate / rotate -->
      <plugin filename="TransformControl" name="Transform control">
        <ignition-gui>
          <property key="resizable" type="bool">false</property>
          <property key="width" type="double">250</property>
          <property key="height" type="double">50</property>
          <property key="state" type="string">floating</property>
          <property key="showTitleBar" type="bool">false</property>
          <property key="cardBackground" type="string">#777777</property>
        </ignition-gui>

        <!-- disable legacy features used to connect this plugin to GzScene3D -->
        <legacy>false</legacy>
      </plugin>

      <!-- Entity tree -->
      <plugin filename="EntityTree" name="Entity tree">
        <ignition-gui>
          <property type="string" key="state">docked_collapsed</property>
        </ignition-gui>
      </plugin>

      <!-- Inspector -->
      <plugin filename="ComponentInspector" name="Component inspector">
        <ignition-gui>
          <property type="string" key="state">docked</property>
        </ignition-gui>
      </plugin>

      <!-- Map -->
      <plugin filename="NavSatMap" name="NavSat Map">
        <ignition-gui>
          <title>NavSat Map</title>
          <property key="state" type="string">docked</property>
        </ignition-gui>
        <topic>/navsat</topic>
        <topic_picker>true</topic_picker>
      </plugin>
    </gui>

    <light type="directional" name="sun">
    </light>

    <!-- Track -->
    <include>
        <uri>race_track_1</uri>
    </include>

    <!-- gokart -->
    <model name="navigator_agk" canonical_link="chassis">
        <link name="chassis">
            <pose relative_to="__model__">0.00 0.00 0.36 0.0000 0.0000 0.0000</pose>
            <inertial>
                <mass>90.7184</mass>
                <inertia>
                    <ixx>9.130349739263998</ixx>
                    <ixy>0</ixy>
                    <ixz>0</ixz>
                    <iyy>17.563242206778668</iyy>
                    <iyz>0</iyz>
                    <izz>21.074915183418668</izz>
                </inertia>
            </inertial>
            <visual name="visual">
                <geometry>
                    <box>
                        <size>1.397 0.914 0.610</size>
                    </box>
                </geometry>
            </visual>
            <collision name="collision">
                <geometry>
                    <box>
                        <size>1.397 0.914 0.610</size>
                    </box>
                </geometry>
            </collision>
            <sensor name="navsat" type="navsat">
              <always_on>1</always_on>
              <update_rate>1</update_rate>
              <topic>navsat</topic>
            </sensor>
        </link>
        <link name="left_kingpin">
            <pose relative_to="chassis">0.52 0.33 -0.23 0.0000 0.0000 0.0000</pose>
            <visual name="visual">
                <geometry>
                    <cylinder>
                        <radius>0.0254</radius>
                        <length>0.0762</length>
                    </cylinder>
                </geometry>
            </visual>
        </link>
        <link name="right_kingpin">
            <pose relative_to="chassis">0.52 -0.33 -0.23 0.0000 0.0000 0.0000</pose>
            <visual name="visual">
                <geometry>
                    <cylinder>
                        <radius>0.0254</radius>
                        <length>0.0762</length>
                    </cylinder>
                </geometry>
            </visual>
        </link>
        <link name="front_left_wheel">
            <pose relative_to="chassis">0.52 0.53 -0.23 -1.5708 0.0000 0.0000</pose>
            <inertial>
                <mass>2.26796</mass>
                <inertia>
                    <ixx>0.011301673229941666</ixx>
                    <ixy>0</ixy>
                    <ixz>0</ixz>
                    <iyy>0.011301673229941666</iyy>
                    <iyz>0</iyz>
                    <izz>0.01650669198655</izz>
                </inertia>
            </inertial>
            <visual name="visual">
                <geometry>
                    <cylinder>
                        <radius>0.12065</radius>
                        <length>0.127</length>
                    </cylinder>
                </geometry>
            </visual>
            <collision name="collision">
                <geometry>
                    <cylinder>
                        <radius>0.12065</radius>
                        <length>0.127</length>
                    </cylinder>
                </geometry>
                <surface>
                    <friction>
                        <ode>
                            <mu>0.5</mu>
                            <mu2>1.0</mu2>
                            <fdir1>0 0 1</fdir1>
                        </ode>
                    </friction>
                </surface>
            </collision>
        </link>
        <link name="front_right_wheel">
            <pose relative_to="chassis">0.52 -0.53 -0.23 -1.5708 0.0000 0.0000</pose>
            <inertial>
                <mass>2.26796</mass>
                <inertia>
                    <ixx>0.011301673229941666</ixx>
                    <ixy>0</ixy>
                    <ixz>0</ixz>
                    <iyy>0.011301673229941666</iyy>
                    <iyz>0</iyz>
                    <izz>0.01650669198655</izz>
                </inertia>
            </inertial>
            <visual name="visual">
                <geometry>
                    <cylinder>
                        <radius>0.12065</radius>
                        <length>0.127</length>
                    </cylinder>
                </geometry>
            </visual>
            <collision name="collision">
                <geometry>
                    <cylinder>
                        <radius>0.12065</radius>
                        <length>0.127</length>
                    </cylinder>
                </geometry>
                <surface>
                    <friction>
                        <ode>
                            <mu>0.5</mu>
                            <mu2>1.0</mu2>
                            <fdir1>0 0 1</fdir1>
                        </ode>
                    </friction>
                </surface>
            </collision>
        </link>
        <link name="rear_left_wheel">
            <pose relative_to="chassis">-0.52 0.58 -0.23 -1.5708 0.0000 0.0000</pose>
            <inertial>
                <mass>3.175144</mass>
                <inertia>
                    <ixx>0.024421673656554996</ixx>
                    <ixy>0</ixy>
                    <ixz>0</ixz>
                    <iyy>0.024421673656554996</iyy>
                    <iyz>0</iyz>
                    <izz>0.025605948788000002</izz>
                </inertia>
            </inertial>
            <visual name="visual">
                <geometry>
                    <cylinder>
                        <radius>0.127</radius>
                        <length>0.20955</length>
                    </cylinder>
                </geometry>
            </visual>
            <collision name="collision">
                <geometry>
                    <cylinder>
                        <radius>0.127</radius>
                        <length>0.20955</length>
                    </cylinder>
                </geometry>
                <surface>
                    <friction>
                        <ode>
                            <mu>0.5</mu>
                            <mu2>1.0</mu2>
                            <fdir1>0 0 1</fdir1>
                        </ode>
                    </friction>
                </surface>
            </collision>
        </link>
        <link name="rear_right_wheel">
            <pose relative_to="chassis">-0.52 -0.58 -0.23 -1.5708 0.0000 0.0000</pose>
            <inertial>
                <mass>3.175144</mass>
                <inertia>
                    <ixx>0.024421673656554996</ixx>
                    <ixy>0</ixy>
                    <ixz>0</ixz>
                    <iyy>0.024421673656554996</iyy>
                    <iyz>0</iyz>
                    <izz>0.025605948788000002</izz>
                </inertia>
            </inertial>
            <visual name="visual">
                <geometry>
                    <cylinder>
                        <radius>0.127</radius>
                        <length>0.20955</length>
                    </cylinder>
                </geometry>
            </visual>
            <collision name="collision">
                <geometry>
                    <cylinder>
                        <radius>0.127</radius>
                        <length>0.20955</length>
                    </cylinder>
                </geometry>
                <surface>
                    <friction>
                        <ode>
                            <mu>0.5</mu>
                            <mu2>1.0</mu2>
                            <fdir1>0 0 1</fdir1>
                        </ode>
                    </friction>
                </surface>
            </collision>
        </link>
        <joint name="fl_steer_joint" type="revolute">
            <parent>chassis</parent>
            <child>left_kingpin</child>
            <axis>
                <xyz>0.00 0.00 1.00</xyz>
            </axis>
        </joint>
        <joint name="fr_steer_joint" type="revolute">
            <parent>chassis</parent>
            <child>right_kingpin</child>
            <axis>
                <xyz>0.00 0.00 1.00</xyz>
            </axis>
        </joint>
        <joint name="fl_wheel_joint" type="revolute">
            <parent>left_kingpin</parent>
            <child>front_left_wheel</child>
            <axis>
                <xyz>0.00 0.00 1.00</xyz>
            </axis>
        </joint>
        <joint name="fr_wheel_joint" type="revolute">
            <parent>right_kingpin</parent>
            <child>front_right_wheel</child>
            <axis>
                <xyz>0.00 0.00 1.00</xyz>
            </axis>
        </joint>
        <joint name="rl_wheel_joint" type="revolute">
            <parent>chassis</parent>
            <child>rear_left_wheel</child>
            <axis>
                <xyz>0.00 0.00 1.00</xyz>
            </axis>
        </joint>
        <joint name="rr_wheel_joint" type="revolute">
            <parent>chassis</parent>
            <child>rear_right_wheel</child>
            <axis>
                <xyz>0.00 0.00 1.00</xyz>
            </axis>
        </joint>
        <plugin
            filename="ignition-gazebo-ackermann-steering-system"
            name="ignition::gazebo::systems::AckermannSteering">
            <left_joint>fl_wheel_joint</left_joint>
            <left_joint>rl_wheel_joint</left_joint>
            <right_joint>fr_wheel_joint</right_joint>
            <right_joint>rr_wheel_joint</right_joint>
            <left_steering_joint>fl_steer_joint</left_steering_joint>
            <right_steering_joint>fr_steer_joint</right_steering_joint>
            <kingpin_width>0.6604</kingpin_width>
            <steering_limit>0.5</steering_limit>
            <wheel_base>1.0414</wheel_base>
            <wheel_separation>1.0668</wheel_separation>
            <wheel_radius>0.254</wheel_radius>
            <min_velocity>-1</min_velocity>
            <max_velocity>4</max_velocity>
            <min_acceleration>-3</min_acceleration>
            <max_acceleration>3</max_acceleration>
        </plugin>
    </model>
  </world>
</sdf>