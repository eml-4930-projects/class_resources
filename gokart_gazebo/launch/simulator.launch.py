import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

"""
    **Not to be used. Incomplete**

    * How does ROS2 Launch know where my gazebo world is?
        Within the ign_gazebo.launch.py you can specifiy the absolute path to the directory
        containing the models.
"""
def generate_launch_description():

    class_launch = get_package_share_directory('class_launch')

    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(class_launch, 'launch', 'gazebo.launch.py')),
        launch_arguments={
            'gazebo_args': 'farm.sdf --gui-config /home/patrick/harvest_ws/src/harv-gazebo/farm/harvester.config'
        }.items(),
    )

    """
    # Need a tf2 between map and odom coordinate frames
    odom_tf_pub = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(get_package_share_directory('ign_gazebo_tools'), 'launch', 'tf_publisher.launch.py'))
    )

    rviz = Node(
       package='rviz2',
       executable='rviz2',
       arguments=['-d', os.path.join(class_launch, 'rviz', 'harv_simulation.rviz')],
       condition=IfCondition(LaunchConfiguration('rviz')),
    )
    """

    """
        The meaning of the symbols after the ROS message type
        @ is a bidirectional bridge.
        [ is a bridge from Ignition to ROS.
        ] is a bridge from ROS to Ignition.
    """
    """
    bridge = Node(
        package='ros_ign_bridge',
        executable='parameter_bridge',
        arguments=['/model/vb7/cmd_vel@geometry_msgs/msg/Twist]ignition.msgs.Twist',
                   '/f_lidar/points@sensor_msgs/msg/PointCloud2[ignition.msgs.PointCloudPacked',
                   '/r_lidar/points@sensor_msgs/msg/PointCloud2[ignition.msgs.PointCloudPacked',
                   '/model/vb7/odometry@nav_msgs/msg/Odometry[ignition.msgs.Odometry',
                   '/world/farm/model/vb7/joint_state@sensor_msgs/msg/JointState[ignition.msgs.Model',
                   '/model/vb7/pose@geometry_msgs/msg/PoseStamped[ignition.msgs.Pose',
                   '/clock@rosgraph_msgs/msg/Clock[ignition.msgs.Clock'],
        output='screen'
    )
    """

    """
        Not currently used, relies on a URDF but ignition gazebo
        uses sdf to desribe robots and I don't feel like creating 
        a converter.
    rsp = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        output='screen',
        # parameters=[params]
    )
    """

    """
        Note on static transform inputs:
        x, y, z, yaw, pitch ,roll
    """
    # This transform is dependant on the transform set in the farm.sdf for the starting 
    # point of the harvester
    map_to_odom_tf = Node(
        package='tf2_ros',
        namespace='sim',
        executable='static_transform_publisher',
        name='map_to_odom',
        arguments=["0", "10.0", "0", "0.261799", "0", "0", "map", "odom"],
        parameters=[{'use_sim_time': True}]
    )

    flidar_to_chassis_tf = Node(
        package='tf2_ros',
        namespace='sim',
        executable='static_transform_publisher',
        name='f_lidar_to_chassis',
        arguments=["4.3434", "0", "0", "0", "0", "0", "vb7/chassis", "vb7/chassis/f_lidar"],
        parameters=[{'use_sim_time': True}]
    )

    rlidar_to_chassis_tf = Node(
        package='tf2_ros',
        namespace='sim',
        executable='static_transform_publisher',
        name='r_lidar_to_chassis',
        arguments=["-4.3434", "0", "0", "3.14159", "0", "0", "vb7/chassis", "vb7/chassis/r_lidar"],
        parameters=[{'use_sim_time': True}]
    )

    return LaunchDescription([
        gazebo,
        DeclareLaunchArgument('rviz', default_value='true',
                              description='Open RViz.'),
        bridge,
        rviz,
        odom_tf_pub,
        map_to_odom_tf,
        flidar_to_chassis_tf,
        rlidar_to_chassis_tf
    ])
