"""Launch Ignition Gazebo with command line arguments."""

import os
from glob import glob

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import ExecuteProcess
from launch.substitutions import LaunchConfiguration

from ament_index_python.packages import get_package_share_directory

def generate_launch_description():
    
    """ 
    Grabs all the directory paths under gokart_gazebo/gazebo to be added
    to the GAZEBO_RESOURCE_PATH environment variable.
    """
    gazebo_class_paths = glob(os.path.join(get_package_share_directory('gokart_gazebo'), 'gazebo/*/'))

    env = { 'IGN_GAZEBO_SYSTEM_PLUGIN_PATH':
            os.pathsep.join([os.environ.get('IGN_GAZEBO_SYSTEM_PLUGIN_PATH', default=''),
                     os.environ.get('LD_LIBRARY_PATH', default='')]),
            'IGN_GAZEBO_RESOURCE_PATH':
            os.pathsep.join([os.environ.get('IGN_GAZEBO_RESOURCE_PATH', default='')] +
                        gazebo_class_paths)}

    return LaunchDescription([
        DeclareLaunchArgument('ign_args', default_value='',
                              description='Arguments to be passed to Ignition Gazebo'),
        ExecuteProcess(
            cmd=['ign gazebo',
                 LaunchConfiguration('ign_args'),
                 ],
            output='screen',
            additional_env=env,
            shell=True
        )
    ])
